package br.com.gpe.resource;

import java.time.LocalDate;

import javax.validation.constraints.NotBlank;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FiltroDataModel {
	private String nome;
    private String cpf;
    private String email;
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private LocalDate dataNascimento;    
}
