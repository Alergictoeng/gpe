package br.com.gpe.resource;

import java.time.LocalDate;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.br.CPF;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequisicaoCadastroDataModel {
	
	@JsonIgnore
	private Long uuid;
	
	@NotBlank(message = "{msg.nome.embranco}")
    @Size(max=150, message = "{msg.nome.tamanho}")
	@NotNull(message = "{msg.nome.obrigatorio}")
	private String nome;
	
	@NotBlank(message = "{msg.cpf.invalido}")
	@NotNull(message = "{msg.cpf.obrigatorio}")
	private String cpf;
    
	@Size(max=400, message = "{msg.email.tamanho}")
	@Email(message = "{msg.email.invalido}")
	@NotBlank(message = "{msg.email.invalido}")
	@NotNull(message = "{msg.email.obrigatorio}")
	private String email;
    
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private LocalDate dataNascimento;
	
	@JsonIgnore
	private Boolean emAtividade;
}
