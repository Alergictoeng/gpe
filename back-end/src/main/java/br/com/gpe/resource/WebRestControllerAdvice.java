package br.com.gpe.resource;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.server.ResponseStatusException;

import br.com.gpe.exceptions.ApiErro;

public class WebRestControllerAdvice {

	@ExceptionHandler(ResponseStatusException.class)
	public ApiErro handleNotFoundException(ResponseStatusException ex) {
		ApiErro responseMsg = new ApiErro(ex.getStatus().value(), ex.getMessage());
		return responseMsg;
	}
	
}
