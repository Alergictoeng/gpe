package br.com.gpe.resource;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import br.com.gpe.core.out.PessoaPersistencePort;
import br.com.gpe.core.util.ImagemUtil;
import br.com.gpe.exceptions.EntidadeEncontradaException;
import br.com.gpe.exceptions.EntidadeNaoEncontradaException;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;

import static org.springframework.http.HttpStatus.*;

import java.io.IOException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/cadastros")
@Api("cadastros")
public class GerenciamentoController {

	@Autowired
	private PessoaPersistencePort port;

	public GerenciamentoController(PessoaPersistencePort port) {
		this.port = port;
	}

	@PostMapping
	@ResponseStatus(CREATED)
	public RespostaCadastroDataModel criar(@RequestBody @Valid RequisicaoCadastroDataModel dataModel ){
		try {
			return port.salva(dataModel);
		}catch (EntidadeEncontradaException e) {
			throw new ResponseStatusException(BAD_REQUEST,e.getMessage());
		}catch (IllegalArgumentException e) {
			throw new ResponseStatusException(INTERNAL_SERVER_ERROR,e.getMessage());
		}	   
	}
	
	@PostMapping("/imagens")
	@ResponseStatus(CREATED)
	public void upload(@RequestParam MultipartFile arquivo){
		try {
			port.salvarImagem(arquivo);
		}catch (EntidadeEncontradaException e) {
			throw new ResponseStatusException(BAD_REQUEST,e.getMessage());
		}catch (IOException e) {
			throw new ResponseStatusException(INTERNAL_SERVER_ERROR,e.getMessage());
		}catch (IllegalArgumentException e) {
			throw new ResponseStatusException(INTERNAL_SERVER_ERROR,e.getMessage());
		}	   
	}
	
	@GetMapping(value = "/imagens/{uuid}",
			    produces = MediaType.IMAGE_JPEG_VALUE)
	public byte[] download(@PathVariable Long uuid){
		try {
			return port.buscarPorImagem(uuid);		
		}catch (EntidadeEncontradaException e) {
			throw new ResponseStatusException(BAD_REQUEST,e.getMessage());
		}catch (IOException e) {
			throw new ResponseStatusException(INTERNAL_SERVER_ERROR,e.getMessage());
		}catch (IllegalArgumentException e) {
			throw new ResponseStatusException(INTERNAL_SERVER_ERROR,e.getMessage());
		}		
	}

	@PatchMapping("{uuid}")
	@ResponseStatus(NO_CONTENT)
	public void atualizar(@PathVariable Long uuid,
					     @RequestBody @Valid RequisicaoCadastroDataModel dataModel ){
		try {
			port.atualiza(uuid,dataModel);
		}catch (EntidadeNaoEncontradaException e) {
			throw new ResponseStatusException(NOT_FOUND,e.getMessage());
		}catch (EntidadeEncontradaException e) {
			throw new ResponseStatusException(NOT_FOUND,e.getMessage());
		}catch (Exception e) {
			throw new ResponseStatusException(INTERNAL_SERVER_ERROR,e.getMessage());
		}
	}

	@DeleteMapping("{uuid}")
	@ResponseStatus(NO_CONTENT)
	public void desativar(@PathVariable Long uuid){
		try {
			port.desativar(uuid);
		}catch (EntidadeNaoEncontradaException e) {
			throw new ResponseStatusException(NOT_FOUND,e.getMessage());
		}catch (Exception e) {
			throw new ResponseStatusException(INTERNAL_SERVER_ERROR,e.getMessage());
		}				
	}

	@GetMapping("{uuid}")
	public RespostaCadastroDataModel obterPorUuid(@PathVariable Long uuid){
		try {
			return port.obtemPorUuid(uuid);		
		}catch (EntidadeNaoEncontradaException e) {
			throw new ResponseStatusException(NOT_FOUND,e.getMessage());
		}catch (Exception e) {
			throw new ResponseStatusException(INTERNAL_SERVER_ERROR,e.getMessage());
		}		
	}
	
	@GetMapping
	 public List<RespostaCadastroDataModel> buscar(FiltroDataModel filtro){
		try {
			return port.buscarPor(filtro);		
		}catch (EntidadeNaoEncontradaException e) {
			throw new ResponseStatusException(NOT_FOUND,e.getMessage());
		}catch (Exception e) {
			throw new ResponseStatusException(INTERNAL_SERVER_ERROR,e.getMessage());
		}		
	}
	
}
