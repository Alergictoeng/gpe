package br.com.gpe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class GpeApplication {

	public static void main(String[] args) {
		SpringApplication.run(GpeApplication.class, args);
	}

}
