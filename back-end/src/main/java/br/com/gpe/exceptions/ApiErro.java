package br.com.gpe.exceptions;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApiErro {
	  private String messagem;
	  private int status;
	 
	  public ApiErro(int status, String mensagem){
		  this.status = status;
		  this.messagem = mensagem;		  
	  }
	  
	}
