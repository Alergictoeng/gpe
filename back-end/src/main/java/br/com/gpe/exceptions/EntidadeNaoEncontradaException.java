package br.com.gpe.exceptions;

public class EntidadeNaoEncontradaException extends RuntimeException {
	public EntidadeNaoEncontradaException(String mensagem) {
		super(mensagem);
	}
}
