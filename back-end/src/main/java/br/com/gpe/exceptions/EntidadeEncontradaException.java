package br.com.gpe.exceptions;

public class EntidadeEncontradaException extends RuntimeException {
	public EntidadeEncontradaException(String mensagem) {
		super(mensagem);
	}
}
