package br.com.gpe.persistence;

import java.time.LocalDate;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "imagem")
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Setter(value = AccessLevel.PACKAGE)
@Getter
public class ImagemJpaEntity {
    // A estratégia de increment pois o postgres não tem uma estratégia similar
	// ao auto_increment.
	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
    @Column(name = "id")
    private Long id;
    
	@NotNull
    @Column(name = "uuid")
    private Long uuid;

    @NotNull
    @Column(name = "content_type")
    private String formato;
    // Normalmente para esse tipo de operação eu uso @Lob, 
    // porém para abreviar tempo do projeto e evitar configuração extra que o Postgres demanda
    // optei por uma coluna de tamanho fixo.
    @NotNull
    @Basic(fetch = FetchType.LAZY)
	@Column(name = "arquivo", length = 16000000)
	private byte[] arquivo;   

}
