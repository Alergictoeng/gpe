package br.com.gpe.persistence;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface PessoaJpaRepository extends JpaRepository<PessoaJpaEntity, Integer> {
	
	PessoaJpaEntity findByCpf(@Param("cpf") String cpf);
	
	PessoaJpaEntity findByUuid(@Param("uuid") Long uuid);
	
	@Query(" select p from PessoaJpaEntity p where p.uuid = :uuid and p.emAtividade = true ")
	Optional<PessoaJpaEntity> findByUuidEmAtividade(@Param("uuid") Long uuid);
}
