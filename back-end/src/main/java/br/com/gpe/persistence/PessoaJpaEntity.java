package br.com.gpe.persistence;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "pessoa")
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Setter(value = AccessLevel.PACKAGE)
@Getter
public class PessoaJpaEntity {
	// A estratégia de increment pois o postgres não tem uma estratégia similar
	// ao auto_increment.	
    @Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
    @Column(name = "id")
    private Long id;
    
    @Column(name = "uuid")
    @NotNull
    private Long uuid;
    
    @Column(name = "nome", length = 150)
    @NotNull
    private String nome;
    
    @Column(name = "cpf", length = 11)
    @NotNull
    private String cpf;
    
    @Column(name = "email", length = 400)
    @NotNull
    private String email;

    @Column(name = "data_nascimento")
    private LocalDate dataNascimento;
    
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "imagem_id", referencedColumnName = "id")
	private ImagemJpaEntity imagem;
    
    @Column(name = "em_atividade")
    @NotNull
    private Boolean emAtividade;
    
}