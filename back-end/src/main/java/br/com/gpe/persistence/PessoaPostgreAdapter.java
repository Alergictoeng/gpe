package br.com.gpe.persistence;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

import javax.activation.MimetypesFileTypeMap;

import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.io.Files;

import br.com.gpe.core.out.PessoaPersistencePort;
import br.com.gpe.core.util.Conversor;
import br.com.gpe.core.util.ImagemUtil;
import br.com.gpe.core.util.ValidaCPF;
import br.com.gpe.exceptions.EntidadeEncontradaException;
import br.com.gpe.exceptions.EntidadeNaoEncontradaException;
import br.com.gpe.resource.FiltroDataModel;
import br.com.gpe.resource.RequisicaoCadastroDataModel;
import br.com.gpe.resource.RespostaCadastroDataModel;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class PessoaPostgreAdapter implements PessoaPersistencePort{

	private final PessoaJpaRepository repository;

	@Override
	public RespostaCadastroDataModel salva(RequisicaoCadastroDataModel requisicao) throws EntidadeEncontradaException, IllegalArgumentException{
		Long uuidNovo;
		
		boolean isCpfValido = ValidaCPF.isCPF(
								ValidaCPF.somenteNumeros(requisicao.getCpf()));
		if(!isCpfValido)
		   throw new IllegalArgumentException("Numero de CPF Inválido.");
		
		PessoaJpaEntity pessoaCPF = repository.findByCpf(requisicao.getCpf());
		if(pessoaCPF != null)
			throw new EntidadeEncontradaException("CPF já Cadastrado.");
		
		do {
			uuidNovo = gerarUuid();
		}while(!(repository.findByUuid(uuidNovo) == null));

		PessoaJpaEntity pessoa = 
				new PessoaJpaEntity()
				.builder()
				.cpf(ValidaCPF.somenteNumeros(requisicao.getCpf()))
				.nome(requisicao.getNome())
				.email(requisicao.getEmail())
				.dataNascimento(requisicao.getDataNascimento())
				.emAtividade(true)
				.uuid(uuidNovo)
				.build();
		
		repository.save(pessoa);
		
		requisicao.setUuid(pessoa.getUuid());
		requisicao.setEmAtividade(pessoa.getEmAtividade());

		return new Conversor().respostaApartirDa(requisicao);
		
	}

	private long gerarUuid() {
		return Long.parseUnsignedLong(Long.toString(~ThreadLocalRandom.current().nextLong()).replace("-",""));
	}

	@Override
	public void atualiza(Long uuid, RequisicaoCadastroDataModel viewM) throws EntidadeEncontradaException, EntidadeNaoEncontradaException, IllegalArgumentException{
		
		PessoaJpaEntity pessoaCPF = repository.findByCpf(ValidaCPF.somenteNumeros(viewM.getCpf()));
		if(pessoaCPF == null)
			throw new EntidadeNaoEncontradaException("CPF não cadastrado");
		if(pessoaCPF.getUuid().longValue() != uuid.longValue())
			throw new EntidadeEncontradaException("O CPF ("+viewM.getCpf()+") não pertence a este usuário");
		
		
		Optional<PessoaJpaEntity> pessoaUuid = repository.findByUuidEmAtividade(uuid);
		pessoaUuid.map( pessoalocal -> {			
			pessoalocal.setCpf(ValidaCPF.somenteNumeros(viewM.getCpf()));
			pessoalocal.setNome(viewM.getNome());
			pessoalocal.setEmail(viewM.getEmail());
			pessoalocal.setDataNascimento(viewM.getDataNascimento());			
			repository.save(pessoalocal);
			return pessoalocal;
		}).orElseThrow(() -> new EntidadeNaoEncontradaException("Usuário não encontrado.") );
		
	}

	@Override
	public void desativar(Long uuid) throws EntidadeNaoEncontradaException, IllegalArgumentException{
		Optional<PessoaJpaEntity> pessoaUuid = repository.findByUuidEmAtividade(uuid);
		pessoaUuid.map( pessoalocal -> {
			pessoalocal.setEmAtividade(false);
			repository.save(pessoalocal);
			return pessoalocal;
		}).orElseThrow(() -> new EntidadeNaoEncontradaException("Usuário não encontrado.") );
	}

	@Override
	public RespostaCadastroDataModel obtemPorUuid(Long uuid) throws EntidadeNaoEncontradaException, IllegalArgumentException{

		Optional<PessoaJpaEntity> pessoaUuid = repository.findByUuidEmAtividade(uuid);
		PessoaJpaEntity entidadeJpa = pessoaUuid.map(
				pessoalocal -> {return pessoalocal;}
				).orElseThrow(() -> new EntidadeNaoEncontradaException("Usuário não encontrado.") );
		
		return new Conversor().respostaApartirDa(entidadeJpa);
	}

	@Override
	public List<RespostaCadastroDataModel> buscarPor(FiltroDataModel filtro)
			throws EntidadeNaoEncontradaException, IllegalArgumentException {
		
		if(filtro.getCpf() != null) {
			boolean isCpfValido = ValidaCPF.isCPF(
					ValidaCPF.somenteNumeros(filtro.getCpf()));
			if(!isCpfValido)
				throw new IllegalArgumentException("Numero de CPF Inválido.");
		}
		
		ExampleMatcher matcher = ExampleMatcher
				.matching()
				.withIgnoreCase()
				.withStringMatcher(
						ExampleMatcher.StringMatcher.CONTAINING );
		Example example = Example.of(new Conversor().entidadeJpaApartirDo(filtro), matcher);

		List<PessoaJpaEntity> entrada = repository.findAll(example); 

		List<RespostaCadastroDataModel> saida;
		saida = entrada.stream()
				.map(entidadeJpa -> 
				new Conversor().respostaApartirDa(entidadeJpa)
						).collect(Collectors.toList());		
		return saida;
	}

	@Override
	public void salvarImagem(MultipartFile arquivo) throws EntidadeNaoEncontradaException, IllegalArgumentException, IOException{
		
		validaTipoETamanhoArquivo(arquivo);
		
		String uuidBruta = retiraExtencao(arquivo.getOriginalFilename());
		
		if(uuidBruta == null || uuidBruta.isEmpty() || uuidBruta.isBlank()) 
				throw new IllegalArgumentException("O nome deve ser a UUID do usuário.");
				
		ImagemJpaEntity imagem = new ImagemJpaEntity().toBuilder()
				.uuid(Long.parseLong(uuidBruta))
				.formato(arquivo.getContentType())
				.arquivo(new ImagemUtil().compressao(arquivo.getBytes()))
				.build();
		         
		Optional<PessoaJpaEntity> pessoaUuid = repository.findByUuidEmAtividade(imagem.getUuid());
		pessoaUuid.map( pessoalocal -> {			
			pessoalocal.setImagem(imagem);			
			repository.save(pessoalocal);
			return pessoalocal;
		}).orElseThrow(() -> new EntidadeNaoEncontradaException("Usuário não encontrado.") );

	}
	
	 private void validaTipoETamanhoArquivo(MultipartFile arquivo) throws IOException, IllegalArgumentException{
		 File file = convertMultiPartToFile(arquivo);
		 MimetypesFileTypeMap fileTypeMap = new MimetypesFileTypeMap();
		 String mimeType = fileTypeMap.getContentType(file.getName());
		 switch (mimeType) {
		 case "image/png":
			 break;
		 case "image/bmp":
			 break;
		 case "image/jpeg":
			 break;
		 case "image/gif":
			 break;
		 default:
			 throw new IllegalArgumentException("Formato de arquivos aceitos png,bmp,jpeg e gif");			 
		 }
	}
	 
	 private File convertMultiPartToFile(MultipartFile file ) throws IOException{
	        File convFile = new File( file.getOriginalFilename() );
	        FileOutputStream fos = new FileOutputStream( convFile );
	        fos.write( file.getBytes() );
	        fos.close();
	        return convFile;
	 }

	static String retiraExtencao (String str) {
		 String resultado = ValidaCPF.somenteNumeros(str);
	        if (resultado == null) 
	        	throw new IllegalArgumentException("O nome deve ser a UUID do usuário.");
	        
	        if (resultado.indexOf(".") > 0)
	        	resultado += resultado.substring(0, resultado.lastIndexOf("."));
	        
	        return resultado;
    }

	@Override
	public byte[] buscarPorImagem(Long uuid) throws EntidadeNaoEncontradaException, IllegalArgumentException, IOException {
		
		Optional<PessoaJpaEntity> pessoaUuid = repository.findByUuidEmAtividade(uuid);
		
		ImagemJpaEntity imagemPersistida = pessoaUuid.map(
				pessoalocal -> {return pessoalocal.getImagem();}
				).orElseThrow(() -> new EntidadeNaoEncontradaException("Imagem não encontrada.") );
		
		
		return new ImagemUtil().descompressao(imagemPersistida.getArquivo());
	}

}
