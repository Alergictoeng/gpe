package br.com.gpe.core.util;

import br.com.gpe.persistence.PessoaJpaEntity;
import br.com.gpe.resource.FiltroDataModel;
import br.com.gpe.resource.RequisicaoCadastroDataModel;
import br.com.gpe.resource.RespostaCadastroDataModel;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class Conversor {

	public RespostaCadastroDataModel respostaApartirDa(RequisicaoCadastroDataModel requisicao) {
		RespostaCadastroDataModel resp = 
				new RespostaCadastroDataModel(
						Long.toString(requisicao.getUuid()),
						requisicao.getNome(),
						requisicao.getCpf(),
						requisicao.getEmail(),
						requisicao.getDataNascimento(),
						requisicao.getEmAtividade());				
		return resp;
	}

	public RespostaCadastroDataModel respostaApartirDa(PessoaJpaEntity entidadeJpa) {
		RespostaCadastroDataModel resp =
				new RespostaCadastroDataModel(
						Long.toString(entidadeJpa.getUuid()),
						entidadeJpa.getNome(),
						entidadeJpa.getCpf(),
						entidadeJpa.getEmail(),
						entidadeJpa.getDataNascimento(),
						entidadeJpa.getEmAtividade());
		return resp;
	}

	public PessoaJpaEntity entidadeJpaApartirDo(FiltroDataModel filtro) {
		PessoaJpaEntity entity = 
				new PessoaJpaEntity()
				.builder()
				.cpf(ValidaCPF.somenteNumeros(filtro.getCpf()))
				.nome(filtro.getNome())
				.email(filtro.getEmail())
				.dataNascimento(filtro.getDataNascimento())
				.emAtividade(true)
				.build();
		return entity;		
	}
}
