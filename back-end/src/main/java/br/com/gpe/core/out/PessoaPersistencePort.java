package br.com.gpe.core.out;

import java.io.IOException;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import br.com.gpe.exceptions.EntidadeEncontradaException;
import br.com.gpe.exceptions.EntidadeNaoEncontradaException;
import br.com.gpe.resource.FiltroDataModel;
import br.com.gpe.resource.RequisicaoCadastroDataModel;
import br.com.gpe.resource.RespostaCadastroDataModel;

@Service
public interface PessoaPersistencePort {

	RespostaCadastroDataModel salva(RequisicaoCadastroDataModel viewM) throws EntidadeEncontradaException, IllegalArgumentException;

	void atualiza(Long uuid, RequisicaoCadastroDataModel viewM) throws EntidadeEncontradaException, EntidadeNaoEncontradaException, IllegalArgumentException;

	void desativar(Long uuid) throws EntidadeNaoEncontradaException, IllegalArgumentException;

	List<RespostaCadastroDataModel> buscarPor(FiltroDataModel filtro) throws EntidadeNaoEncontradaException, IllegalArgumentException;

	RespostaCadastroDataModel obtemPorUuid(Long uuid);

	void salvarImagem(MultipartFile arquivo) throws EntidadeNaoEncontradaException, IllegalArgumentException, IOException;

	public byte[] buscarPorImagem(Long uuid) throws EntidadeNaoEncontradaException, IllegalArgumentException, IOException;

	
}
