DROP TABLE IF EXISTS 
   pessoa,
   imagem;

 create table pessoa (
        id bigint not null,
        cpf varchar(11) not null,
        data_nascimento date,
        em_atividade boolean not null,
        email varchar(400) not null,
        nome varchar(150) not null,
        uuid bigint not null,
        imagem_id bigint,
        primary key (id)
    )

  create table imagem (
        id bigint not null,
        arquivo blob not null,
        content_type varchar(255) not null,
        uuid bigint not null,
        primary key (id)
    )
   
   ALTER TABLE Orders
   ADD CONSTRAINT fk_imgpess
   FOREIGN KEY (imagem_id) REFERENCES imagem(id);
