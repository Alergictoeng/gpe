# Gerenciamento de Pessoas

O documento abaixo, contem os dados técnicos, analiticos e arquiteturais mais relevantes sobre o Sistema de gerenciamento de pessoas (GPE).

### Importante!!!
#### *Se possível, ultilize um visualizador de Markdonwn para uma melhor experiência.
#### *O Gerenciamento de Pessoas (GPE) não é um sistema com layout bonito, mas cumpre os requisitos funcionais.


## Indice
**1 - Componentes da solução**
*1.1 - Front-end*
*1.1.1 - Estrutura de pacotes*
*1.2 - Back-end*
*1.2.1 - Estrutura de pacotes*
*1.2.2 - Documentação da API*
**2 - Requisitos técnicos**
**3 - Instalação do PostgreSQL**
*3.1 Caso não seja possível instalar o PostgreSQL*
**4 - Clonando o repositório**
**5 - Iniciando o back-end**
**6 - Iniciando o front-end**


## 1 - Componentes da solução
## Por favor antes de prossegir com a leitura, abra o arquivo `diagrama_container.png` na pasta `docs` na raiz do projeto, pois ela dá o contexto do documento abaixo.

### 1.1 - Front-end
Este provê toda a interação visual do usuário o backend.
##### 1.1.1 - Estrutura de pacotes:
1. `atualizar-cadastro`
Pacote que contém todas os arquivos do componente atualização dos dados do cadastro.
2. `criar-cadastro`
Pacote que contém todas os arquivos do componente de criação de cadastro.
3. `detalhes-cadastro`
Pacote que contém todas os arquivos do componente dos detalhes do cadastro.
4. `listagem-cadastro`
Pacote que contém todas os arquivos do componente de listagem dos cadastros.

Para ter acesso ao mesmo deve se digitar no navegador `http://localhost:4200/`

### 1.2 - Back-end
Este provê toda a lógica de negócios para o funcionamento do sistema, existem basicamente duas maneira de interagir com esse componente da solução, a primeira é via Front-end e a segunta é via Endpoitns REST.
##### 1.2.1 - Estrutura de pacotes:
1. `br.com.gpe` 
Pacote que contém todas as configurações da aplicação.
2. `br.com.gpe.out`
Pacote que contém todas as portas da aplicação (portas e adaptadores).
3. `br.com.gpe.util`
Pacote que contém todos os ultilitários (conversores etc..) da aplicação.
4. `br.com.gpe.exceptions`
Pacote que contém todas as exceções modeladas no domínio do negócio.
5. `br.com.gpe.persistence`
Pacote que contém todas as entidades e adaptadores para o banco de dados.
6. `br.com.gpe.resources`
Pacote que contém todos os modelos e controllers dos endpoints.

A aplicação segue o modelo hexagonal (portas e adaptadores), assim propiciando uma melhor manutenabilidade de código e um acoplamento relativamente mais baixo entre os módulos.
**Observação**: o GPE(Gerenciamento de pessoas) em específico, implementa parcialmente o modelo hexagonal, já que este é basicamente um CRUD sem processos de negócio, apenas entrada e saída de dados.
##### 1.2.2 - Documentação da API
Após subir o módulo Back-end, digite no navegador o seguinte endereço para ter acesso a documentação interativa:

`http://localhost:8080/swagger-ui.html/`

A mesma foi feita seguindo o modelo de endpoints implementado pelo github em sua api publica.
`RequisicaoCadastroDataModel` e `RespostaCadastroDataModel`, são os dois modelos de dados criados especificamente para interagir com a API, que tem os seguintes endpoints:

`GET[/api/cadastros]` - Obtem todos os cadastros, porém aceira query parameter para filtro.
`POST[/api/cadastros]` - Cria um novo cadastro.
`GET[/api/cadastros/{uuid}]` - Obtem um cadastro.
`DELETE[/api/cadastros/{uuid}]` - Desativa um cadastro.
`PATCH[/api/cadastros/{uuid}]` - Atualiza um cadastro ( a escolha pelo PATCH em vez do PUT se deu por que o PATCH é ultilizado apenas para atualização parcial do recurso, o que se encaixa muito bem em nossa api.
`POST[/api/cadastros/imagens]` - Faz upload da imagem, esta que deve ter o valor do uuid como nome, por exemplo 123456789.jpg, digamos que (123456789) seja o uuid do usuário.
`GET[/api/cadastros/imagens/{uuid}]` - Faz o download da imagem de um cadastro.

## 2 - Requisitos técnicos
* Java 11
* Maven 3.6.0
* Node 12.18.4
* Angular 10.1.2
* Npm 6.14.6
* Postgres 10.14
* Git 2.17.1

##  3 - Instalação do PostgreSQL
Instalando PostgreSQL no Debian Linux.

    $ sudo apt-get install postgresql  

Esse comando instala os pacotes necessários.

    $ /etc/init.d/postgresql status

Testamos o status do banco para verificar se está tudo certo.

    $ sudo -u postgres psql postgres
    psql (9.5.10)
    Type "help" for help.

    postgres=# \password postgres
    Enter new password: 
    Enter it again: 

Depois da instalação, o usuário postgres com privilégios administrativos é criado com uma senha vazia como padrão. Como primeiro passo temos que criar uma senha pro postgres.   

    $ sudo -u postgres createuser --interactive --password user12
    Shall the new role be a superuser? (y/n) n
    Shall the new role be allowed to create databases? (y/n) y
    Shall the new role be allowed to create more new roles? (y/n) n
    Password: 

Nós criamos o usuário do banco

    $ sudo -u postgres createdb testdb -O user12

Depois criamos o banco testdb com o commando createdb, que aqgora tem o user12 como responsável.

    $ sudo vim /etc/postgresql/<versão do postgres>/main/pg_hba.conf

Nós editamos o arquivo pg_hba.conf

    # "local" is for Unix domain socket connections only
    local   all             all                                     trust
    # IPv4 local connections:
    host    all             all             127.0.0.1/32            trust

Para ser capaz de rodar o Spring Boot com a instalação local do PostgreSQL, nos mudamos o metodo de instalação para "Unix domain socket" e "local connections" para "trust".

    $ sudo service postgresql restart

Reiniciamos o PostgreSQL para habilitar as mudanças.

    $ psql -U user12 -d testdb -W
    Password for user user12: 
    psql (9.5.10)
    Type "help" for help.
    
    testdb=>

agora podemos usar o psql para conectar com o banco.

Fonte de presquisa para a instalação: http://zetcode.com/springboot/postgresql/
### 3.1 Caso não seja possível instalar o PostgreSQL
Caso não seja possível por algum motivo a execução do PostgreSQL, existe a alternativa do banco em memória H2 que pode ser configurado da seguinte forma:
Adicione a seguinte dependencia ao `POM.xml` 

	<dependency>
		<groupId>com.h2database</groupId>
		<artifactId>h2</artifactId>
	</dependency>
Adicione a seguinte configuração ao arquivo `application.properties`

    #Banco de dados de desenvolvimento H2
    spring.datasource.url=jdbc:h2:mem:testdb
    spring.datasource.driverClassName=org.h2.Driver
    spring.datasource.username=sa
    spring.datasource.password=pa
    spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
    spring.h2.console.enabled=true
    spring.h2.console.path=/h2-console
		
Desta forma, os testes de uso do GPE podem correr sem problemas.


##  4 - Clonando o repositório

1. Você pode ver na aba "Source" o botão "Clone" no canto superior direito.
2. Digite ou Cole no terminal o comando que estará no formato "git clone https:<Repositório>.git".
3. Após isso o projeto gpe estará em sua maquina.

##  5 - Iniciando o back-end
1. Verifique se o Postgres está no ar.
2. abra um novo terminal, navegue até /gpe/back-end e digite:

    `$ mvn clean install`
    
3. Esse comando irá baixar e instalar as dependencias do back-end, após a conclusão, digite:

   `$ mvn spring-boot:run`    
 
 Os schemas do banco de dados serão gerados automaticamente para maior comodidade, entretanto se preferir, existe o arquivo `referencia_data.sql` onde residem os scripts de criação.
 
Isso iniciará o nosso back-end 

## 6 - Iniciando o front-end
1. Abra um novo terminal, navegue até /gpe/front-end e digite:
2. Assumindo que ja há instalação do Node e Npm na máquina, verificamos as versões:

    `$ node -v`
    `$ npm -v`

3. Para instalar o Angular CLI 

    `$npm install -g @angular/cli`

5. Checando a versão 

    `$ng --version`

6. Instalação do devkit 

    `$npm install --save-dev @angular-devkit/build-angular`

7. Verifique se o Back-End está no ar.

    `$ ng serve`
    
Isso iniciará o front-end, para ter acesso a view da aplicação digite no navegador:

`http://localhost:4200/`

### Bom uso