import { Subject, Observable } from "rxjs";
import { Component, OnDestroy, OnInit } from "@angular/core";
import { Router } from '@angular/router';
import { Cadastro } from "../cadastro";
import { CadastroService } from "../cadastro.service";
import { DetalhesCadastroComponent } from '../detalhes-cadastro/detalhes-cadastro.component';

@Component({
  selector: "app-listagem-cadastro",
  templateUrl: "./listagem-cadastro.component.html",
  styleUrls: ["./listagem-cadastro.component.css"]
})
export class ListagemCadastroComponent implements OnDestroy, OnInit {
  cadastro: Cadastro[] = [];
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  constructor(private cadastroService: CadastroService,
    private router: Router) {}

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,
      processing: true
    };
    this.cadastroService.listarTodos().subscribe(data => {
      this.cadastro = data;
      this.dtTrigger.next();
    });
  }
	
  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  desativarCadastro(uuid: String) {
    this.cadastroService.desativarCadastro(uuid)
      .subscribe(
        data => {
          console.log(data);
   	  this.listar();
        },
        error => console.log(error));
  }
  
  listar(){
    this.cadastroService.listarTodos().subscribe(data => {
      this.cadastro = data;      
    });
  }
	
  atualizarCadastro(uuid: String){
    this.router.navigate(['atualizarCadastro', uuid]);
  }	

  detalhesCadastro(uuid: String){
    this.router.navigate(['detalhesCadastro', uuid]);
  }
}
