import { TestBed } from '@angular/core/testing';

import { ListagemCadastroService } from './listagem-cadastro.service';

describe('ListagemCadastroService', () => {
  let service: ListagemCadastroService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ListagemCadastroService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
