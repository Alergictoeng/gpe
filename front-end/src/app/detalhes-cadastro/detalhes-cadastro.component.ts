import { Cadastro } from '../cadastro';
import { Component, OnInit, Input } from '@angular/core';
import { CadastroService } from '../cadastro.service';
import { ListagemCadastroComponent } from '../listagem-cadastro/listagem-cadastro.component';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detalhes-cadastro',
  templateUrl: './detalhes-cadastro.component.html',
  styleUrls: ['./detalhes-cadastro.component.css']
})
export class DetalhesCadastroComponent implements OnInit {
  avatar:any
  uuid: string;
  cadastro: Cadastro;

  constructor(private route: ActivatedRoute,private router: Router,
    private cadastroService: CadastroService) { }

  ngOnInit() {
    this.cadastro = new Cadastro();

    this.uuid = this.route.snapshot.params['uuid'];
    
    this.cadastroService.obterCadastro(this.uuid)
      .subscribe(data => {
        console.log(data)
        this.cadastro = data;
      }, error => console.log(error));
	
    this.cadastroService.obterImagem(this.uuid)
      .subscribe((data : any) => {
	var reader = new FileReader();
 	reader.readAsDataURL(data); 
 	reader.onload = () => { 
     	   this.avatar = reader.result; 
    	}	
      }, error => console.log(error));
  }

  listar(){
    this.router.navigate(['listarCadastros']);
  }

}
