import { TestBed } from '@angular/core/testing';

import { DetalhesCadastroService } from './detalhes-cadastro.service';

describe('DetalhesCadastroService', () => {
  let service: DetalhesCadastroService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DetalhesCadastroService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
