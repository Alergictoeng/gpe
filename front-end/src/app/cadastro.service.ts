import { Injectable } from '@angular/core';
import { HttpClient , HttpErrorResponse} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { concatMap, tap, catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class CadastroService {

  imagem: File;

  private baseUrl = 'http://localhost:8080/api/cadastros';

  constructor(private http: HttpClient) { }

  obterCadastro(uuid: String): Observable<any> {
    return this.http.get(`${this.baseUrl}/${uuid}`);
  }
	
  criarCadastro(cadastro: Object, imagem: File): Observable<Object> {
    this.imagem = imagem;
    if (typeof this.imagem != "undefined") {
	    return this.http.post(`${this.baseUrl}`, cadastro)
		.pipe(
		    tap(
			res => {var val = JSON.parse(JSON.stringify(res));}
		    ),
		    concatMap(
			(res: { uuid: string }) => {
				function imagemNomeEExtencao(str){
				  var file = str.split('/').pop();
				  return [file.substr(0,file.lastIndexOf('.')),
					file.substr(file.lastIndexOf('.')+1,file.length)]
				}
				var imagemRenomeada = new File([this.imagem], res.uuid+'.'+imagemNomeEExtencao(this.imagem.name)[1]);
				const uploadImagem = new FormData();
	    		        uploadImagem.append('arquivo', imagemRenomeada);
				return this.http.post(`${this.baseUrl}/imagens`,uploadImagem)
			}
		    ),
		    tap(
			res => console.log('Retorno mensagem', res)
		    )
		);
     }else{
 	return this.http.post(`${this.baseUrl}`, cadastro).pipe(
		      		catchError(this.handleError)
    			);
     		}
  }
  
  atualizarCadastro(uuid: String, valor: any, imagem: File): Observable<Object> {
    this.imagem = imagem;
    if (typeof this.imagem != "undefined") {
    	return this.http.patch(`${this.baseUrl}/${uuid}`, valor)
	.pipe(
            tap(res => {var val = JSON.parse(JSON.stringify(res));}),
            concatMap(
		res => {			
			function imagemNomeEExtencao(str){
			  var file = str.split('/').pop();
			  return [file.substr(0,file.lastIndexOf('.')),
				file.substr(file.lastIndexOf('.')+1,file.length)]
			}
			var imagemRenomeada = new File([this.imagem], uuid+'.'+imagemNomeEExtencao(this.imagem.name)[1]);
		        const uploadImagem = new FormData();
    		        uploadImagem.append('arquivo', imagemRenomeada);
			return this.http.post(`${this.baseUrl}/imagens`,uploadImagem)
		}
	    ),catchError(this.handleError));
    }else{
	return this.http.patch(`${this.baseUrl}/${uuid}`, valor).pipe(
		      		catchError(this.handleError)
    			);
     		}
  }

  obterImagem(uuid: String): Observable<Blob> {  
    return this.http.get(`${this.baseUrl}/imagens/${uuid}`, { responseType: 'blob'});
  }

  desativarCadastro(uuid: String): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${uuid}`, { responseType: 'text' });
  }

  listarTodos(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }
  
  private handleError(error: HttpErrorResponse) {
   let errorMessage = '';
   if (error.error instanceof ErrorEvent) {
     errorMessage = `Ocorreu um Erro: ${error.error.message}`;
   } else {
    errorMessage = `Erro (${error.status})` +`: ${error.error.message}`;
   }
    return throwError(errorMessage);
  }; 
}
