import { CadastroService } from '../cadastro.service';
import { Cadastro } from '../cadastro';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CpfValidator } from '../cpf.validator';

@Component({
  selector: 'app-criar-cadastro',
  templateUrl: './criar-cadastro.component.html',
  styleUrls: ['./criar-cadastro.component.css']
})
export class CriarCadastroComponent implements OnInit {

  cadastro: Cadastro = new Cadastro();
  submitted = false;  
  arquivoMaiorQue1mb = false;
  formatoIncorreto = false;
  imagem: File;
  formulario: FormGroup;
  error: any;

  constructor(private cadastroService: CadastroService,
    private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit() {
       this.formulario = this.formBuilder.group({
            cpf: ['', [Validators.required, CpfValidator.validar]],
            nome: ['', [Validators.required,Validators.maxLength(150)]],
            email: ['', [Validators.required, Validators.email, Validators.maxLength(400)]],
	    dataNascimento: ['']
        });
  }
  get campo() { return this.formulario.controls; }
  
  novoCadastro(): void {
    this.submitted = false;
    this.cadastro = new Cadastro();
  }

  enviar() {
    this.cadastroService
    .criarCadastro(this.cadastro, this.imagem).subscribe(data => {
      this.cadastro = new Cadastro();
      this.listar()
    }, 
    error => this.error = error);
  }

    selecionarImagem(event){
    var imagemLocal = event.target.files[0];
   
    console.log('size', imagemLocal.size);
    console.log('type', imagemLocal.type);
    
    if(imagemLocal.size > 1000000){
	this.arquivoMaiorQue1mb = true;
    }else	
	this.arquivoMaiorQue1mb = false;
   
    switch (imagemLocal.type) {
  	case "image/png":
	    this.formatoIncorreto = false;
	break;
	case "image/bmp":
  	    this.formatoIncorreto = false;
        break;
	case "image/jpeg":
	    this.formatoIncorreto = false;
        break;
	case "image/gif":
	    this.formatoIncorreto = false;
        break;
	default:
	    this.formatoIncorreto = true;
    }

    if(this.arquivoMaiorQue1mb || this.formatoIncorreto){
	return;
    }
    
    this.imagem = imagemLocal;
  }
  
  salvar() {
    this.submitted = true;
    if (this.formulario.invalid) {
        return;
    }
    this.cadastro = this.formulario.value;
    this.enviar();    
  }
  
  limparCampos() {
	this.error = '';
        this.submitted = false;
        this.formulario.reset();
  }
  
  listar(){
    this.router.navigate(['listarCadastros']);
  }

}
