import { TestBed } from '@angular/core/testing';

import { CriarCadastroService } from './criar-cadastro.service';

describe('CriarCadastroService', () => {
  let service: CriarCadastroService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CriarCadastroService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
