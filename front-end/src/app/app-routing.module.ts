import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetalhesCadastroComponent } from './detalhes-cadastro/detalhes-cadastro.component';
import { CriarCadastroComponent } from './criar-cadastro/criar-cadastro.component';
import { ListagemCadastroComponent } from './listagem-cadastro/listagem-cadastro.component';
import { AtualizarCadastroComponent } from './atualizar-cadastro/atualizar-cadastro.component';

const routes: Routes = [
  { path: '', redirectTo: 'listarCadastros', pathMatch: 'full' },
  { path: 'listarCadastros', component: ListagemCadastroComponent },
  { path: 'criarCadastro', component: CriarCadastroComponent },
  { path: 'atualizarCadastro/:uuid', component: AtualizarCadastroComponent },
  { path: 'detalhesCadastro/:uuid', component: DetalhesCadastroComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
