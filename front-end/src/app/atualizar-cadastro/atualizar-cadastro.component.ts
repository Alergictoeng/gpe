import { Component, OnInit } from '@angular/core';
import { Cadastro } from '../cadastro';
import { ActivatedRoute, Router } from '@angular/router';
import { CadastroService } from '../cadastro.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CpfValidator } from '../cpf.validator';

@Component({
  selector: 'app-atualizar-cadastro',
  templateUrl: './atualizar-cadastro.component.html',
  styleUrls: ['./atualizar-cadastro.component.css']
})
export class AtualizarCadastroComponent implements OnInit {
  
  avatar:any
  imagem: File;
  uuid: string;
  cadastro: Cadastro;
  formulario: FormGroup;
  error: any;
  submitted = false; 
  arquivoMaiorQue1mb = false;
  formatoIncorreto = false; 

  constructor(private route: ActivatedRoute,private router: Router,
    private cadastroService: CadastroService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.formulario = this.formBuilder.group({
            cpf: ['', [Validators.required, CpfValidator.validar]],
            nome: ['', [Validators.required,Validators.maxLength(150)]],
            email: ['', [Validators.required, Validators.email, Validators.maxLength(400)]],
	    dataNascimento: ['']
        });
    this.cadastro = new Cadastro();
    this.uuid = this.route.snapshot.params['uuid'];    
    this.cadastroService.obterCadastro(this.uuid)
      .subscribe(data => {
	
	this.cadastro = data;

	this.formulario.patchValue({
         cpf: this.cadastro.cpf,
         nome: this.cadastro.nome,
         email: this.cadastro.email,
         dataNascimento: this.cadastro.dataNascimento
        });

      }, error => this.error = error);
	
    this.cadastroService.obterImagem(this.uuid)
      .subscribe((data : any) => {
	var reader = new FileReader();
 	reader.readAsDataURL(data); 
 	reader.onload = () => { 
     	   this.avatar = reader.result; 
    	}	
      }, error => console.log(error));
  }

  get campo() { return this.formulario.controls; }

  atualizarCadastro() {
    this.cadastroService.atualizarCadastro(this.uuid, this.cadastro, this.imagem)
      .subscribe(data => {
        console.log(data);
        this.cadastro = new Cadastro();
        this.listar();
      }, error => this.error = error);
  }
  
  selecionarImagem(event){
    var imagemLocal = event.target.files[0];
   
    console.log('size', imagemLocal.size);
    console.log('type', imagemLocal.type);
    
    if(imagemLocal.size > 1000000){
	this.arquivoMaiorQue1mb = true;
    }else	
	this.arquivoMaiorQue1mb = false;
   
    switch (imagemLocal.type) {
  	case "image/png":
	    this.formatoIncorreto = false;
	break;
	case "image/bmp":
  	    this.formatoIncorreto = false;
        break;
	case "image/jpeg":
	    this.formatoIncorreto = false;
        break;
	case "image/gif":
	    this.formatoIncorreto = false;
        break;
	default:
	    this.formatoIncorreto = true;
    }

    if(this.arquivoMaiorQue1mb || this.formatoIncorreto){
	return;
    }
    
    this.imagem = imagemLocal;
  }

  onSubmit() {
    this.submitted = true;  
    if (this.formulario.invalid) {
        return;
    }
    this.cadastro = this.formulario.value;
    this.atualizarCadastro();    
  }

  listar() {
    this.router.navigate(['listarCadastros']);
  }
}
